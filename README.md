## Writer Deluxe

Small old project from 2002 (disclaimer: I was 16...). It is a type-writter text 
routine.

Some features were:

* Automatic calculation of the number of lines and the number of words that fit
  in a line. Calculations were dependent on bitmap font size.
* Typing speed was configurable. Punctuation and non punctuation characters
  could be configured with different speeds. It was also possible to have an
  alternate speed setting when pressing a key.
* It could display a background image to the text.
* It supported configuring different background images for different number of lines.

Orignally written for Fenix, it was modified in 2009 to make it work in BennuGD,
just for the fun of it.

Now I have just added some structure and added some script file to make it easier
to run (only a bash script is provided).

![screen-recording](/Media/screen-recording.gif)

### Build & Run

As a pre-requisite, `bgdc` and `bgdi` must be in your PATH.

Bash scripts `./Src/build` and `./Src/run` are provided to build & run the example.

