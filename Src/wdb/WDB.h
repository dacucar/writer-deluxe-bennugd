//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// Si utilizas esta rutina y piensas distribuir los fuentes, por favor, incluye
// las siguientes 6 l�neas en tu c�digo para dar cr�dito. Gracias :).
//-----------------------------------------------------------------------------
//              ESTE PROGRAMA USA WRITER DELUXE (BENNU VERSION)
//                      Copyright (c) Dar�o Cutillas 2002
//-----------------------------------------------------------------------------
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#ifndef _WDB_H
#define _WDB_H

/* 
  NOTA DE LA REVISI�N 2009.07.20 (Bennu)
  Este es un c�digo antiguo y hay numerosas cosas que de hacerlas ahora no 
  estar�an como est�n. La revisi�n no ha sido sino ordenar algunas cosas, 
  modificar algunas partes de la documentaci�n y hacer los cambios necesarios
  para que pueda compilarse con bennu.
  
  Hay numerosas partes del c�digo que podr�an mejorarse, sin embargo, la rutina
  es funcional si se usa de modo apropiado. Se recomienda encarecidamente 
  estudiar el ejemplo proporcionado as� como leer la documentaci�n incluida
  en este archivo
*/

/* 
  NOTA SOBRE EL CONVENIO DE NOMBRES UTILIZADO
   Para no interferir con las variables que tienes declaradas en tu prg,
   todas las variables llevan __r o r como prefijo de los nombres de las mismas
   salvo las variables privadas que hacen de contador (i,a,j,etc). As� mismo, los
   procesos no principales llevan el prefijo wd (writer deluxe).
*/

// -----------------------------------------------------------------------------
// PAR�METROS DE CONFIGURACi�N
// -----------------------------------------------------------------------------
#define __rTT 250   // L�mite de n�mero de caracteres m�ximos que se podr�n 
                    // tener en pantalla al mismo tiempo. Un n�mero alto 
                    // consumir� m�s memoria que uno m�s bajo
  
#define __rTL 100   // N�mero m�ximo de renglones que ocupar� un texto.
                    // Decrementando este n�mero se ahorrar� memoria
// -----------------------------------------------------------------------------                    

/* USO DE LA RUTINA
  A d�a 25/08/02 la rutina cuenta con 4 procesos los cuales se detallan:
    
    � writer_deluxe(__rWDClass pointer __rConf)
      -->Este es el proceso principal de la rutina. Como par�metro hay que 
      pasarle la direcci�n de mem�ria de una variable de tipo __rWDClass.
      Si no sabes lo que significa eso, te dir� que a la variable pasada
      debe preceder el operador &. Ejemplo: writer_deluxe(&mivariable)
      -->El tipo __rWDClass es un tipo definido por el usuario que guarda
      configuraciones que ser�n pasadas al proceso principal. Puede declarar
      variables de este tipo en cualquier sitio donde se puedan declarar
      variables y acceder a sus elementos como si fuese una estructura. 
      Ejemplo: declaramos '__rWDClass mivariable;' en la secci�n global
      y usamos 'mivariable.rTexto="Hola"' o 'mivariable.rEspaciado=10' para 
      modificar sus elemntos. Facil, �no? Cada elemento de el tipo __rWDClass
      est� explicado en su declaraci�n
      -->Por defecto, una variable de tipo __rWDClass se inicializa con unos
      valores determinados.
    
    � wd_load_text(string __rFichero, int __rLine, string pointer __rTXT)
      -->Este proceso permite cargar una l�nea de texto de un fichero de texto
      Como parametros se le debe proporcionar el nombre del fichero, la l�nea
      (la primera se corresponde con el 1) y el offset de la variable rTexto
      de la variable tipo __rWDClass. Ejemplo:
      wd_load_text("c:\texto.txt",5,&mivariable.rTXT) (ver el ejemplo.prg si no
      se entiende)
      
    �   wd_make_effect(__rEfecto)
      -->Este proceso se encarga de hacer los efectos de entrada. No tendr�s
      que llamarlo directamente ya que las variables rUseEffects y rEffects[]
      se encargan de ello. La descripci�n de las mismas se hace en la declaraci�n.
      Los efectos disponibles a fecha de hoy son:
        1 - Tama�o de 0 a 100%. Escala el gr�fico de fondo desde 0 a 100.
        2 - Desplazamiento de izquierda a derecha.
      -->Puedes programar tus propios efectos si sabes. SI lo consideras interesante
      puedes enviarmelo para que lo inlcuya en la rutina :).
    
    �   wd_exit_effect(__rEfecto)
      -->Este proceso tampoco tendr�s que llamarlo directamente. Usa la variable
      rExitEffect para especificar el efecto de salida que se usar�. 
      Efectos disponibles:
        1 - Tama�o de 100% a 0. Escala el gr�fico de fondo desde 100% a 0.
        2 - Desplazamiento hasta salir por la derecha de la pantalla.
      -->Puedes programar tus propios efectos si sabes. Si lo consideras interesante
      puedes enviarmelo para que lo inlcuya en la rutina :).
      
    Es importante tambi�n que establezcas un valor adecuado para las costantes
    __rTT y __rTL (mira la explicaci�n en su declaraci�n)
      
  FAQ:
    �Por qu� no se ven correctamente los caracteres con acentos al cargar un
    archivo de un txt?
      A mi no me mire�s que eso no es culpa m�a si no de fgets. Usar el edit
      de msdos y no tendre�s problemas (los usuarios de windows Xp lo tienen tmb
      Inicio->Ejecutar->Escribir edit y pulsar intro :P
    
    �Para que sirve tener varios gr�ficos?
      He visto en algunos juegos que tienen gr�ficos ajustados al n�mero de lineas
      de tal modo que si el texto ocupa una linea no se quede espacio debajo. Si 
      vas a usar m�s de un gr�fico, debes modificar la declaraci�n de rGraphics[0] 
      por rGraphics[m�ximo de graficos que usaras].
      Aunque tengas varias variables de tipo __rWDClass esto no es problema ya
      que la variable rTotGraphics se encarga de especificar cuantos usaremos.
    
    �Como cargo las fuentes/gr�ficos?
      Usa la load_fnt o load_map o load_png o etc. Ejemplo
        mivariablerWDclass.rGraphics[0]=load_map("mio.map");
    
    �Para que sirven rMargenI y rMargenD?
      Si usas un gr�fico como fondo, seguramente no querr�s que el texto comience
      en la cordenada 0,0 del gr�fico... Con esto evitas el problema.
      
    �Las velocidades como van?
      En las variables de control de velocidad a mayor n�mero, menor velocidad.
      Realmente deber�a haberlas llamado lentitud.. Prueba distintos n�mero para
      ver que velocidad se adapta a tus necesidades.
    
    �Para que sirve lo del IntroChar?
      Digamos que es como el caracter de retorno y avance de l�nea. Sirve para
      hacer puntos y a parte. Por ejemplo, si rIntrochar="&" y rTexto= "Hola &guapa"
      se imprimiria:
                Hola
                guapa
      Se debe poner precediendo a la palabra que aparecer� en la siguiente l�nea
    
    �Tengo que tener algo de cuidado al escribir las cadenas?
      Pues aunque la rutina est� preparada para calcular cuantas l�neas ocupar� 
      el texto, comprueba si hay un espacio cuando empieza nueva l�nea evitando
      que quede mal, y otras muchas comprobaciones debes tener cuidado con dos
      cosas:
        1- No usar palabras m�s largas que el n�mero especificado en rMaxCaract,
          no he probado lo que ocurrir�a...
        2- Lo de comprobar si el espacio est� al principio de linea, funciona
          siempre y cuando no haya mas de uno espacios seguidos... 
          es decir, se ver�an todos menos 1 :P. 
      Usa el sentido com�n
    
    No me he enterao de nada de lo que pone, �que hago?
      Mirar el c�digo de ejemplo.prg, leer la descripci�n que acompa�a a la 
      declaraci�n de las variables en el tipo __rWClass.
      Como �ltimo recurso enviame un mail a lord_danko@users.sourceforge.net
    
    Tengo algunas ideas...
      Yo tambi�n, a decir verdad la versi�n proxima ser� la 1.0 o muy cercana
      y las mejoras que tengo pensadas son bastante chulas :P.
      Enviame tus ideas, mejoras y cr�ticas a lord_danko@users.sourceforge.net
      
    Esta rutina no me gusta y me parece una porquer�a
      No la uses
    
    La he usado, me gusta mucho y quiero agradecertelo (xDD)
      Basta con que me envies un mail diciendomelo :).
*/

import "mod_key";
import "mod_map";
import "mod_video";
import "mod_screen";
import "mod_proc";
import "mod_mem";
import "mod_text";
import "mod_timers";
import "mod_string";
import "mod_file";

type __rWDClass
  rFuente=0;              // fuente que se usar�
  
  string rTexto="";       // texto a escribir
  
  string rIntroChar="&";  // caracter de retorno (para simular la tecla especificada
                          // por rIntroKey ya que los textos cargados se cogen de 
                          // una sola l�nea
  
  rIntroKey=0;            // c�digo ascii de la tecla que se usa para continuar escribiendo
                          // usar 0 para cualquier tecla.
  
  rMaxCaract=20;          // caracteres por l�nea
  rMaxRenglones=3;        // m�ximo de renglones por parrafo
  
  rEspaciado=0;           // distancia entre caracteres (adicional al ancho de cada letra)
  rEspacios=2;            // distancia adicional de los espacios (huecos entre palabras)
  rDistY=0;               // dist�ncia entre l�neas (adicional al tama�o de una letra 
                          // may�scula
  
  rVel=10;                // velocidad general
  rVelPto=100;            // velocidad de pausa despu�s de un punto
  rVelComa=40;            // velocidad de pausa despu�s de coma 
  rVelPtoComa=22;         // velocidad de pausa despu�s de punto y coma
  
  rPosx=0;                // coordenada X de donde se escribir� (o se pondr� la imagen)
  rPosy=0;                // coordenada Y de donde se escribir� (o se pondr� la imagen)
  
  rMargenI=0;             // margen izquierdo desde rPosX hasta donde comienza el texto
  rMargenS=0;             // margen superior desde rPosy hasta donde comienza el texto
  
  rTotGraphics=0;         // n�mero de gr�ficos que se usar�n 0 para ninguno. Si se quiere
                          // un gr�fico para cuando hay una l�nea y otro para lo dem�s,
                          // se deber�a poner un 2 (ejemplo). SI se quiere 1 solo gr�fico
                          // para todo, debe valer 1.
  
  rGraphics[0];           // array para cargar los distintos gr�ficos. Van del siguiente
                          // modo: gr�fico para una l�nea rGraphics[1], gr�fico para 2
                          // l�neas rGraphics[2], etc. El gr�fico que se muestra cuando
                          // no hay disponible para X l�neas es el 0.
            
  byte rUseEffects=0;     // 0=No usar efectos de entrada. >0 Usar efectos de entrada
  rEffects[1]=0,0;        // array de efectos. 1 elemento: 0=no trasparente,>0 trasparente
                          // 2 elemento: 0 no usar efecto de entrada, >0 n�mero del 
                          // efecto de entrada a usar.  
  
  rExitEffect=0;          // 0=no usar efectos de salida. >0 n�mero del efecto a usar
end 

#endif
