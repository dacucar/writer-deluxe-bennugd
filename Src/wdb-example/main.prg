// Ejemplo de WDB (Writer Deluxe Bennu Version)
// NOTA: La documentaci�n est� incluida en el archivo WDB.h

//-----------------------------------------------------------------------------
//              ESTE PROGRAMA USA WRITER DELUXE (BENNU VERSION)
//                 Copyright (c) Dar�o Cutillas 2002 - 2009
//                    lord_danko @ users.sourceforge.net
//                               2009.07.20
//-----------------------------------------------------------------------------

program ejemplo_writer_deluxe;

include "WDB.h";

import "mod_key";

global
  __rWDClass wd;
end

include "WDB.prg";

begin
  set_mode (640, 480, 16);
  FULL_SCREEN = False;

  // configuramos algunos de los parametros y cargamos la fuente y las im�genes
  // observa que el tipo de datos __rWDClass tiene muchos m�s par�metros que 
  // pueden ser configurados
  wd.rFuente = load_fnt("./mensajes.fnt");
  wd.rTotGraphics = 1;
  wd.rGraphics[0] = load_map("./mensajes.map"); 
  wd.rMaxCaract = 35;
  wd.rPosY = 100;
  wd.rMargenI = 10; wd.rMargenI = 10;
  wd.rUseEffects = 1;
  wd.rEffects[0] = 1; //trasparente
  wd.rEffects[1] = 2; //desplazamiento
  wd.rExitEffect = 2; //desplazamiento de salida
  wd_load_text("./texto.txt",1, &wd.rTexto);
  
  writer_deluxe(&wd); //invocamos al proceso writer_deluxe
        
  repeat
    frame;
  until(key(_esc))
end    

